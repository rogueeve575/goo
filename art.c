
#include "goo.h"
#include "art.fdh"

SDL_Surface *art = NULL;
SDL_Surface *logo = NULL;
int logo_width = 1, logo_height = 1;

// the graphics bitmaps converted to C code
#include "ART_RES.H"
#include "LOGO_RES.H"

void GetRGB(SDL_PixelFormat *fmt, uchar b1, uchar b2, uchar b3, uchar *r, uchar *g, uchar *b)
{
int colour;

	colour = b1 | ((int)b2 << 8) | ((int)b3 << 16);
	*r = (uchar)((colour & fmt->Rmask) >> fmt->Rshift) << fmt->Rloss;
	*g = (uchar)((colour & fmt->Gmask) >> fmt->Gshift) << fmt->Gloss;
	*b = (uchar)((colour & fmt->Bmask) >> fmt->Bshift) << fmt->Bloss;
}
int GetColor(uchar red, uchar green, uchar blue)
{
uint value;
SDL_PixelFormat *fmt = screen->format;

	value = ((red >> fmt->Rloss) << fmt->Rshift) |
			((green >> fmt->Gloss) << fmt->Gshift) |
			((blue >> fmt->Bloss) << fmt->Bshift);
    return value;
}
Uint16 RGB(int hexcol)
{
uchar r,g,b;
	r = (hexcol & 0xff0000) >> 16;
	g = (hexcol & 0xff00) >> 8;
	b = (hexcol & 0xff);
	return GetColor(r, g, b);
}

// scale the temp surface temp 3x and return a new surface.
SDL_Surface *ScaleSurface(SDL_Surface *insfc_org)
{
SDL_Surface *insfc, *outsfc, *final;
uint32_t *srcline, *dstline;
int x, y;

	int inw = insfc_org->w;
	int inh = insfc_org->h;
	int outw = inw * 3;
	int outh = inh * 3;

	// input surface to 32bpp
	insfc = SDL_CreateRGBSurface(SDL_SRCCOLORKEY, \
								inw, inh, \
								32,\
								0xff000000, \
								0x00ff0000, \
								0x0000ff00, \
								0x000000ff);
	
	SDL_BlitSurface(insfc_org, NULL, insfc, NULL);
	
	// create output surface at 32bpp
	outsfc = SDL_CreateRGBSurface(SDL_SRCCOLORKEY, \
								outw, outh, \
								32,\
								0xff000000, \
								0x00ff0000, \
								0x0000ff00, \
								0x000000ff);
	
	SDL_LockSurface(insfc);
	SDL_LockSurface(outsfc);
	
	// scale surface
	#define GET_SURFACE_LINE(SFC, LINE)	\
		((void *)(((uint8_t *)SFC->pixels) + (LINE * SFC->pitch)))
	
	for(y=0;y<inh;y++)
	{
		uint32_t *srcptr = GET_SURFACE_LINE(insfc, y);
		uint32_t *dstptr = GET_SURFACE_LINE(outsfc, y*3);
		uint32_t *dst_start = dstptr;
		
		for(x=0;x<inw;x++)
		{
			*(dstptr++) = *srcptr;
			*(dstptr++) = *srcptr;
			*(dstptr++) = *(srcptr++);
		}
		
		memcpy((uint8_t *)dst_start+outsfc->pitch, dst_start, outsfc->pitch);
		memcpy((uint8_t *)dst_start+outsfc->pitch*2, dst_start, outsfc->pitch);
	}
	
	SDL_UnlockSurface(insfc);
	SDL_UnlockSurface(outsfc);
	
	// convert to display format
	SDL_SetColorKey(outsfc, SDL_SRCCOLORKEY|SDL_RLEACCEL,
						SDL_MapRGB(outsfc->format, 0, 0, 0x21));

	final = SDL_DisplayFormat(outsfc);
	SDL_FreeSurface(insfc);
	SDL_FreeSurface(outsfc);
	return final;
}

/*
// this original implementation bit-rotted, replaced it with above implementation
SDL_Surface *ScaleSurface(SDL_Surface *temp)
{
SDL_Surface *sfc, *out;
int x, y, dx, dy;
int c;
unsigned char *srcptr;
uint *dstptr;
uchar r, g, b;
int index_src, index_dst;

	sfc = SDL_CreateRGBSurface(SDL_SRCCOLORKEY, temp->w*3, temp->h*3, 16, \
		screen->format->Rmask, screen->format->Gmask, screen->format->Bmask, \
		screen->format->Amask);
	if (!sfc)
	{
		printf("ScaleSurface: cannot create surface\n");
		return NULL;
	}
	
	SDL_LockSurface(sfc);
	SDL_LockSurface(temp);
	
	printf("ScaleSurface: input sheet width/height = %d x %d\n", temp->w, temp->h);
	srcptr = temp->pixels;
	dstptr = sfc->pixels;
	for(y=dy=0;y<temp->h;y++)
	{
		for(x=dx=0;x<temp->w;x++)
		{
			index_src = (y * temp->pitch) + (x * temp->format->BytesPerPixel);
			index_dst = (dy * (sfc->pitch / 2)) + (dx * (sfc->format->BytesPerPixel/2));
			
			GetRGB(temp->format, srcptr[index_src],
					srcptr[index_src+1], srcptr[index_src+2], &r, &g, &b);
			
			c = GetColor(r, g, b);
			
			dstptr[index_dst] = c;
			dstptr[index_dst+1] = c;
			dstptr[index_dst+2] = c;
			index_dst += sfc->pitch/2;
			dstptr[index_dst] = c;
			dstptr[index_dst+1] = c;
			dstptr[index_dst+2] = c;
			index_dst += sfc->pitch/2;
			dstptr[index_dst] = c;
			dstptr[index_dst+1] = c;
			dstptr[index_dst+2] = c;
			dx += 3;
		}
		dy += 3;
	}
	
	SDL_UnlockSurface(sfc);
	SDL_UnlockSurface(temp);
	
	// setup colorkey for transparency
	SDL_SetColorKey(sfc, SDL_SRCCOLORKEY|SDL_RLEACCEL,
						SDL_MapRGB(screen->format, 0, 0, 0x21));
	printf("scale ok\n");
	
	out = SDL_DisplayFormat(sfc);
	SDL_FreeSurface(sfc);
	return out;
}*/

SDL_Surface *loadgraphic(unsigned char *buf, int buf_size)
{
SDL_Surface *temp, *sfc;
SDL_RWops *rw;

	rw = SDL_RWFromMem(buf, buf_size);
	if (!rw)
	{
		printf("RWFromMem failed\n");
		return NULL;
	}
	temp = SDL_LoadBMP_RW(rw, 0);
	SDL_FreeRW(rw);
	
	if (!temp) { printf("graphic load failure\n"); return NULL; }
	
	sfc = ScaleSurface(temp);
	SDL_FreeSurface(temp);
	
	return sfc;
}

char loadart(void)
{
	art = loadgraphic(art_res, ART_RES_SIZE);
	if (!art) return 1;
	return 0;
}

char load_logo(void)
{
	logo = loadgraphic(logo_res, LOGO_RES_SIZE);
	if (!logo) return 1;
	
	logo_width = logo->w;
	logo_height = logo->h;
	return 0;
}


void fill(int x1, int y1, int x2, int y2, uint c)
{
SDL_Rect rect;

	rect.x = x1; rect.y = y1;
	rect.w = (x2-x1)+1; rect.h = (y2-y1)+1;
	SDL_FillRect(screen, &rect, c);
}

void draw(int x, int y, int t)
{
SDL_Rect srcrect, dstrect;
#define ART_ROWS		3
	dstrect.x = x;
	dstrect.y = y;
	
	srcrect.x = (t % ART_ROWS) * 24;
	srcrect.y = (t / ART_ROWS) * 24;
	srcrect.w = 24;
	srcrect.h = 24;
	
	SDL_BlitSurface(art, &srcrect, screen, &dstrect);
}

void drawlogo(int x, int y)
{
SDL_Rect dstrect;

	dstrect.x = x;
	dstrect.y = y;
	
	SDL_BlitSurface(logo, NULL, screen, &dstrect);
}

void freelogo(void)
{
	if (logo)
	{
		SDL_FreeSurface(logo);
		logo = NULL;
	}
}

void freeart(void)
{
	SDL_FreeSurface(art);
}
