
#include "goo.h"
#include "synth.h"
#include "snd.fdh"

void snd(int s)
{
int ch = s;
	
	switch(s)
	{
		case FIRE:
			tone(ch, 1500, MAX_VOLUME/4, WAVE_SQUARE);
			tone_sweep(ch, -20);
			tone_fade(ch, -30);
		break;
		
		case PDIE:
			tone(ch, 90, MAX_VOLUME/4, WAVE_NOISE);
			tone_fade(ch, -4);
		break;
		
		case EMTINK:
			tone(ch, 2100, MAX_VOLUME/4, WAVE_SQUARE);
			tone_fade(ch, -60);
		break;
		
		case LEVELUP:
			tone(ch, 1000, MAX_VOLUME/4, WAVE_SQUARE);
			tone_sweep(ch, 20);
			tone_fade(ch, -10);
		break;
		
		case EMDIE:
			tone(ch, 1500, MAX_VOLUME/4, WAVE_NOISE);
			tone_sweep(ch, -20);
			tone_fade(ch, -30);
		break;
		
		case SWITCHSIDES:
			tone(ch, 100, MAX_VOLUME/4, WAVE_SQUARE);
			tone_sweep(ch, 5);
			tone_fade(ch, -3);
		break;
		
		case CANWARPNOW:
			tone(ch, 2000, MAX_VOLUME/6, WAVE_SQUARE);
			tone_sweep(ch, 1);
			tone_fade(ch, -3);
		break;
		
		case CANTWARP:
			tone(ch, 100, MAX_VOLUME/8, WAVE_SQUARE);
			tone(CANTWARP2, 100, MAX_VOLUME/8, WAVE_NOISE);
			tone_fade(ch, -8);
			tone_fade(CANTWARP2, -8);
		break;
		
		case PAUSESND:
			tone(ch, 500, MAX_VOLUME/4, WAVE_TRIANGLE);
			tone_fade(ch, -4);
		break;
	}
}

void tone(int ch, int fq, int vol, int type)
{	
	synth_set_wavetype(ch, type);
	tone_on(ch, fq, vol);
}
