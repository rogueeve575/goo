
// implements a starfield-like scrolling thing to give the impression
// of moving water

#include <SDL/SDL.h>
#include "goo.h"
#include "aquafld.fdh"

#define MAX_DOTS		85
#define NUM_DEPTHS		3
#define UPPERDEPTH		2
#define UPPERENTRYPOINT	(-4<<CSF)
#define LOWERENTRYPOINT ((SCREEN_HEIGHT+4) << CSF)
struct
{
	int x, y;		// y is CSFed, x is not
	int speed;
	uchar depth;
} dots[MAX_DOTS];

int num_dots;
int dotspawntimer;

// how many dots there are on the different levels
uchar dots_level[10] = { 16, 18, 20, 25, 30, 35, 40, 45, 60, MAX_DOTS };
uchar dot_sprites[4] = { 47, 56, 59 };
int dot_speeds[4] = { 32, 64, 128 };

void fld_init(void)
{
int i;
	num_dots = dots_level[0];
	for(i=0;i<num_dots;i++)
	{
		initdot(i, rand()%SCREEN_WIDTH, (rand()%SCREEN_HEIGHT)<<CSF);
	}
	settimer(dotspawntimer);
}

void fld_run(void)
{
int i;
	// add more dots as the game levels up
	if (timeup(dotspawntimer, 60))
	{
		settimer(dotspawntimer);
		if (num_dots < dots_level[game.level])
		{
			spawndot(num_dots++);
		}
	}
	
	// move the dots
	for(i=0;i<num_dots;i++)
	{
		if (game.dir==UP)
		{
			if (dots[i].speed > -dot_speeds[dots[i].depth])
			{
				dots[i].speed--;
			}
			if (dots[i].y < UPPERENTRYPOINT)
			{
				spawndot(i);
			}
		}
		else
		{
			if (dots[i].speed < dot_speeds[dots[i].depth])
			{
				dots[i].speed++;
			}
			if (dots[i].y > LOWERENTRYPOINT)
			{
				spawndot(i);
			}
		}
		dots[i].y += dots[i].speed;
	}
}

void fld_draw_lower(void)
{
int i;
	for(i=0;i<num_dots;i++)
	{
		if (dots[i].depth < UPPERDEPTH)
			draw(dots[i].x, dots[i].y>>CSF, dot_sprites[dots[i].depth]);
	}
}

void fld_draw_upper(void)
{
int i;
	for(i=0;i<num_dots;i++)
	{
		if (dots[i].depth >= UPPERDEPTH)
			draw(dots[i].x, dots[i].y>>CSF, dot_sprites[dots[i].depth]);
	}
}

static void initdot(int d, int x, int y)
{
int depth = rand()%NUM_DEPTHS;
	dots[d].x = x;
	dots[d].y = y;
	dots[d].depth = depth;
	dots[d].speed = dot_speeds[depth];
	if (game.dir==UP) dots[d].speed = -dots[d].speed;
}

static void spawndot(int d)
{
	initdot(d, rand()%SCREEN_WIDTH, (game.dir==UP)?LOWERENTRYPOINT:UPPERENTRYPOINT);
}
