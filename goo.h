
#include <stdio.h>
#include <SDL/SDL.h>

#include "snd.h"

#define uchar		uint8_t
#define uint		uint16_t

#ifdef WIN32
	#pragma warning(disable:4761)
#endif

extern SDL_Surface *screen;

#define SCREEN_WIDTH		800
#define SCREEN_HEIGHT		600

typedef struct
{
	int firetimer;
	char dead;
	int diedatx;
	int respawntimer;
	char blinkon, blinking;
	int blinktimesleft;
	int blinkanim;
	uchar lives;
	int frame;
	char zooming;
	char zoomdir;
} player;
extern player p;

typedef struct
{
	int level;			// current game level (0 based)
	int levelcounter;	// counts number of enemies killed this level
	int enemies_killed;	// total kills by player
	int emtimer;		// for enemies to appear at bottom
	int score, highscore;
	
	char over;			// game-over
	// for final score tally
	int scorechars;
	int score_x;
	int gameovertimer;
	int scoretally;
	char scoreblinkstate;
	char scorefmt[8];
	int scoreblinkcount;
	char dir;
	
	// "warp bubble" text
	char warpt_show;
	int warpt_timer;
	char warpt_flashtimesleft;
	
	char paused;
	int pause_blinktimer;
	int pause_blinkstate;
} stgame;
extern stgame game;

#define UP			0
#define DOWN		1

#define PLAY		0
#define TITLE		1
extern int gamemode;

#define GAME_MAX_LEVEL			 9		// actually shown as level 10
#define EMS_NEEDED_FOR_LEVELUP	 25
//#define PTS_FOR_DYING			-50
#define PTS_TO_WARP				400
#define P_BLINK_TIMES			20
#define P_RESPAWN_TIME			2150

#define P_DOWNFRAME				0
#define P_UPFRAME				48
#define P_ZOOMDOWNFRAME			49
#define P_ZOOMUPFRAME			50

typedef struct
{
	int x, y;
	uchar type;
	uchar exists;
	
	char animframe;
	int animtimer;
	
	int health;
	int flags[2];
} object;
#define MAX_OBJECTS		100
extern object obj[MAX_OBJECTS];
extern int po;			// slot used by player

#define CSF				5

extern uchar keys[SDLK_LAST], lastkeys[SDLK_LAST];
extern int mousex, mousey, mousel, mouser;
extern int lmousex, lmousey, lmousel, lmouser;
extern int curtick;

#define settimer(X) { X = curtick; }
#define timeup(X, MS) ( (curtick - X >= MS) || X==-1 )
#define expiretimer(X) { X = -1; }

#define EM_GREEN		0
#define EM_RED			1
#define EM_PSHOT		2
#define EM_PLAYER		3
#define EM_PEXPLODE		4
#define EM_EMEXPLODE	5
#define EM_BUBL_SML		6
#define EM_BUBL_BIG		7
#define EM_SKULL		8
#define EM_TINKSPARK	9
#define EM_PZOOM		10			// only used on title screen

#define BGCOL			0x101061	// background color of the water

#define GetNumberTile(N)	( (N + 21) )
#define NUMBR_SPACING		16
#define TILE_SPACING		24

#define P_W				(24<<CSF)
#define P_H				(24<<CSF)
#define P_TOPPOS		(20<<CSF)
#define P_BTMPOS		((SCREEN_HEIGHT << CSF) - P_TOPPOS - P_H)
#define MAXPX			((SCREEN_WIDTH << CSF) - P_W)
#define MINPX			0
#define MOUSE_ADJ		50

Uint16 RGB(int hexcol);		// added 10/25/2018 K.Shaw with reluctance to change historical code; eliminate implicit function definition
