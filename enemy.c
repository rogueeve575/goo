
#include "goo.h"
#include "enemy.fdh"

char is_enemy[15] =    {1,1,0,0,0,0,0,0,1,0,0};
char em_health[15] =   {1,1,0,0,0,0,0,0,5,0,0};
uchar em_sprites[15] = {1,4,3,0,6,9,12,13,31,33,P_ZOOMUPFRAME};
uchar em_vfsprit[15] = {51,54,53,0,0,0,0,0,57,0,0};
uchar em_animlen[15] = {1,1,0,0,2,2,0,0,1,2,0};
int em_animspd[15] = {200,200,0,0,400,100,0,0,150,100,0};
int em_killpts[15] = {  15,10,0,0,0,0,0,0,100,0,-PTS_TO_WARP };
int ems[4] = { EM_RED, EM_GREEN, EM_SKULL, -1 };

int spawn(int t, int x, int y)
{
int i;
	for(i=0;i<MAX_OBJECTS;i++)
	{
		if (!obj[i].exists)
		{
			obj[i].x = x;
			obj[i].y = y;
			obj[i].type = t;
			obj[i].exists = 1;
			obj[i].health = em_health[t];
			memset(obj[i].flags, 0, sizeof(obj[i].flags));
			settimer(obj[i].animtimer);
			obj[i].animframe = 0;
			switch(obj[i].type)
			{
				case EM_GREEN:
				case EM_RED:
					swimmer_init(i);
					break;
			}
			return i;
		}
	}
	printf("spawn: failed to create obj %d at %d,%d\n", t, x, y);
	fflush(stdout);
	return 0;
}

void kill(int o)
{
	if (o==po)		// player killed
	{
		if (p.blinking) return;
		
		spawn(EM_PEXPLODE, obj[o].x, obj[o].y);
		p.diedatx = obj[po].x;
		po = -1;
		p.dead = 1;
		p.lives--;
		snd(PDIE);
		//if (p.lives) addscore(PTS_FOR_DYING);
		settimer(p.respawntimer);
	}
	obj[o].exists = 0;
}

void obj_del_all(void)
{
int i;
	for(i=0;i<MAX_OBJECTS;i++) obj[i].exists = 0;
}

void doobjects(void)
{
int i;
	for(i=0;i<MAX_OBJECTS;i++)
	{
		if (obj[i].exists)
		{
			if (gamemode == PLAY)
			{
				switch(obj[i].type)
				{
					case EM_PSHOT: pshot_do(i); break;
					
					case EM_BUBL_SML: case EM_BUBL_BIG:
						bubl_do(i);
					break;
					
					case EM_EMEXPLODE:
						obj[i].y -= 80;
					break;
					case EM_TINKSPARK:
						obj[i].y -= 130;
					break;
					
					case EM_GREEN:
					case EM_RED:
					case EM_SKULL:
						swimmer_do(i);
						break;					
				}
			}
			
			if (timeup(obj[i].animtimer, em_animspd[obj[i].type]))
			{
				if (++obj[i].animframe > em_animlen[obj[i].type])
				{
					obj[i].animframe = 0;
					switch(obj[i].type)
					{
						case EM_EMEXPLODE:
						{
							int t = EM_BUBL_SML;
							if (rand()%3==0) t = EM_BUBL_BIG;
							spawn(t, obj[i].x, obj[i].y);
						}
						case EM_TINKSPARK:
						case EM_PEXPLODE:
							kill(i); break;
					}
				}
				settimer(obj[i].animtimer);
			}
		}
	}
}

void drawobjs(void)
{
int i;
	for(i=0;i<MAX_OBJECTS;i++)
	{
		if (obj[i].exists)
		{
			if (obj[i].type != EM_PLAYER)
			{
				int s = em_sprites[obj[i].type];
				if (game.dir==DOWN && em_vfsprit[obj[i].type])
					s = em_vfsprit[obj[i].type];
					
				draw(obj[i].x>>CSF, obj[i].y>>CSF, s+obj[i].animframe);
			}
			else if (p.blinkon)
			{
				draw(obj[i].x>>CSF, obj[i].y>>CSF, p.frame);
			}
		}
	}
}

int emtimer;
int emspawnrate[10] = { 200, 190, 180, 170, 150, 140, 100, 80, 50, 18 };
void spawnem(void)
{
	if (timeup(emtimer, emspawnrate[game.level]) && rand()%3==0)
	{
		int x = rand()%SCREEN_WIDTH;
		// only spawn skulls on level 3 or higher
		int t = ems[rand()%((game.level>=2)?3:2)];
		spawn(t, x<<CSF, (game.dir==UP)?SCREEN_HEIGHT<<CSF:-(24<<CSF));
		settimer(emtimer);
	}
}

void pshot_do(int o)
{
int i;
	if (game.dir==UP)
	{
		obj[o].y += 280;
		if (obj[o].y > SCREEN_HEIGHT<<CSF) { kill(o); return; }
	}
	else
	{
		obj[o].y -= 280;
		if (obj[o].y < -24<<CSF) { kill(o); return; }
	}
	
	for(i=0;i<MAX_OBJECTS;i++)
	{
		if (obj[i].exists && is_enemy[obj[i].type])
		{
			if (intersect(i, o))
			{
				kill(o);
				if (--obj[i].health <= 0)
				{
					spawn(EM_EMEXPLODE, obj[i].x, obj[i].y);
					snd(EMDIE);
					game.enemies_killed++;
					addscore(em_killpts[obj[i].type]);
					kill(i);
					if (++game.levelcounter >= EMS_NEEDED_FOR_LEVELUP)
					{
						game.levelcounter = 0;
						if (game.level < GAME_MAX_LEVEL)
						{	// level up
							game.level++;
							snd(LEVELUP);
						}
					}
				}
				else
				{
					spawn(EM_TINKSPARK, obj[i].x, obj[i].y);
					snd(EMTINK);
				}
				break;
			}
		}
	}
}

#define spd		flags[0]
#define xvect	flags[1]
void swimmer_init(int o)
{
int pox = p.dead?p.diedatx:obj[po].x;
int dist = abs(obj[o].x - pox);

	obj[o].xvect = dist / 170;
	if (obj[o].x > pox && !p.dead) obj[o].xvect = -obj[o].xvect;
}

void swimmer_do(int o)
{
	if (game.dir==UP)
	{
		if (obj[o].y < -25<<CSF) { kill(o); return; }
		if (obj[o].spd > -120) obj[o].spd -= 3;
	}
	else
	{
		if (obj[o].y > SCREEN_HEIGHT<<CSF) { kill(o); return; }
		if (obj[o].spd < 120) obj[o].spd += 3;
	}
	
	obj[o].y += obj[o].spd;
	obj[o].x += obj[o].xvect;
	
	if (!p.dead && intersect(o, po))	// touched player?
		kill(po);		
}

void bubl_do(int o)
{
	obj[o].y -= 100;
	if (obj[o].y < -25<<CSF) kill(o);
}

char intersect(int o1, int o2)
{
int rect1x2, rect1y2, rect2x2, rect2y2;
	rect1x2 = obj[o1].x + (24<<CSF);
	rect1y2 = obj[o1].y + (24<<CSF);
	rect2x2 = obj[o2].x + (24<<CSF);
	rect2y2 = obj[o2].y + (24<<CSF);
	
	if ((obj[o1].x < obj[o2].x) && (rect1x2 < obj[o2].x)) return 0;
	if ((obj[o1].x > rect2x2) && (rect1x2 > rect2x2)) return 0;
	if ((obj[o1].y < obj[o2].y) && (rect1y2 < obj[o2].y)) return 0;
	if ((obj[o1].y > rect2y2) && (rect1y2 > rect2y2)) return 0;
	
	return 1;
}
