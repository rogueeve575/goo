
#include <Application.h>
#include <Roster.h>
#include <Path.h>
#include <Entry.h>

extern "C"
{
	void MatchCurDir();
};

void MatchCurDir()
{
	// set current directory to that of our executable, so we will work from Tracker
	BApplication app("application/x-vnd.Kt-AquaInvaders");
	app_info info;
	BPath myPath;
	
	app.GetAppInfo(&info);
	BEntry entry(&info.ref);
	entry.GetPath(&myPath);
	myPath.GetParent(&myPath);
	
	const char *path = myPath.Path();
	if (path)
		chdir(path);
}
