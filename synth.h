
#define WAVE_SQUARE		0
#define WAVE_TRIANGLE	1
#define WAVE_NOISE		2
#define WAVE_SAW		3
#define WAVE_SINE		4
#define NUM_WAVE_TYPES	5
#define NUM_CHANNELS	10


	char synth_init(void);
	void synth_close(void);
	void synth_run(void);

	void tone_on(uchar c, unsigned int freq, int vol);
	void tone_off(uchar c);
	void synth_set_vol(uchar c, int vol);
	void tone_sweep(uchar channel, int speed);
	void tone_fade(uchar channel, int speed);

	void synth_set_wavetype(uchar c, char w);
	void synth_set_duty(uchar c, uchar duty);
	char channel_in_use(uchar c);
	char synth_get_free_channel(void);
	char synth_vol_applied(uchar c);


#define SYNTH_MIN_FREQ		1
#define SYNTH_MAX_FREQ		5500

// sweeps alter the frequency by the sweep_speed per this many ms.
#define SWEEP_RATE		12
// fades alter the volume by fade_speed every this many ms.
#define FADE_RATE		5

#define DUTY_STEPS		8
#define DUTY_MASK		(DUTY_STEPS-1)
#define DUTY_12			1			// 0 only
#define DUTY_25			2			// 0,1
#define DUTY_33			3			// 0,1,2
#define DUTY_50			4			// 0,1,2,3
#define DUTY_75			DUTY_25		// remove me

#define MAX_VOLUME			4096
#define MIN_VOLUME			-4096
#define WAVE_RANGE			(MAX_VOLUME - MIN_VOLUME)
