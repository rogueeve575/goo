
#include "goo.h"
#include "title.fdh"

extern int logo_width, logo_height;			// <-- from art.c
#define LOGO_WD		(logo_width)
#define LOGO_HT		(logo_height)
#define LOGO_X		((SCREEN_WIDTH / 2) - (LOGO_WD / 2))
#define LOGO_STARTY	-19
#define LOGO_ENDY	50

#define TXT_SPAC	80
#define TXT_WD		((24*6)-45)
#define EM_WD		(TXT_SPAC + TXT_WD)
#define EM_X		((SCREEN_WIDTH / 2) - (EM_WD / 2))
#define EM_TEXTX	(EM_X + TXT_SPAC)
#define EM_APPEAR_RATE	650
#define TEXT_UPDATE_RATE	200

extern int em_killpts[14];
extern int ems[14];
int enemies[14];

#define START		0
#define SHOWENEMIES	1
#define SHOWTEXT	2
#define BLANKTEXT	3
struct
{
	char state;
	int logoy;
	int logomovetimer;
	int movetspd;
	
	int emviscount;
	int texttimer;
	int emtimer;
	int emindex;
	int textindex;
	char emtextstate;
	char copyright_visible;
	
	int showtexttimer;
	int textupdrate;
	char ptsbuf_set_up;
	int showtexttime;
	
	int pushfiretimer;
	int pushfirevis;
} title;
#define TXT_NAMES	0
#define TXT_POINTS	1
char txt_midget[5] = { 66,67,68,0 };
char txt_doomshrimp[7] = { 69,70,71,72,73,74,0 };
char txt_skull[7] = { 75,76,77,78,79,80,0 };
char txt_warp[7] = { 81,82,83,84,85,86,0 };
char txt_ptbuf[4][80];

char *emtext_names[4] = { txt_midget, txt_doomshrimp, txt_skull, txt_warp };
char *emtext_pts[4] = { txt_ptbuf[0], txt_ptbuf[1], txt_ptbuf[2], txt_ptbuf[3] };

char **emtext;

char txt_copyright[7] = { 60,61,62,63,64,65,0 };
#define CPYRT_X	((SCREEN_WIDTH/2)-(6*(TILE_SPACING/2))+1)	// copyright x pos

#define EM_Y		200
#define EM_SPAC		46
int em_y[6] = { EM_Y, EM_Y+EM_SPAC, EM_Y+EM_SPAC+EM_SPAC, EM_Y+(EM_SPAC*4), -1 };

void setemtext(int mode)
{
	if (mode==TXT_NAMES)
	{
		/*emtext[0] = txt_midget;
		emtext[1] = txt_doomshrimp;
		emtext[2] = txt_skull;
		emtext[3] = txt_warp;*/
		emtext = emtext_names;
	}
	else
	{
		if (!title.ptsbuf_set_up) setupenemyptsbuf();
		emtext = emtext_pts;
	}
	title.emtextstate = mode;
}

static void setupenemyptsbuf(void)
{
int i, j;

	for(i=0;i<4;i++)
	{
		char *text = txt_ptbuf[i];
		
		sprintf(text, "%d", em_killpts[enemies[i]]);
		for(j=strlen(text)-1;j>=0;j--)
		{
			text[j] = (text[j]=='-')?17:GetNumberTile(text[j] - '0');
		}
	}
	title.ptsbuf_set_up = 1;
}

void start_title(void)
{
	gamemode = TITLE;
	title.state = START;
	title.ptsbuf_set_up = 0;
	obj_del_all();
	
	title.logoy = LOGO_STARTY;
	settimer(title.logomovetimer);
	
	title.pushfirevis = 0;
	settimer(title.pushfiretimer);
	
	setemtext(TXT_NAMES);
	title.emindex = title.textindex = 0;
	title.showtexttime = 570;
	title.copyright_visible = 0;
	title.textupdrate = EM_APPEAR_RATE;
	title.movetspd = 2;
	memcpy(enemies, ems, sizeof(ems));
	enemies[3] = EM_PZOOM;
	enemies[4] = -1;
	game.dir = UP;
	
	load_logo();
}

void runtitle(void)
{
	// logo
	if (title.logoy < LOGO_ENDY)
	{
		if (timeup(title.logomovetimer, 1))
		{
			settimer(title.logomovetimer);
			title.logoy += title.movetspd;
			title.movetspd = (title.movetspd==1) ? 2:1;
			if (title.logoy >= LOGO_ENDY)
			{
				title.logoy = LOGO_ENDY;
				title.state = SHOWENEMIES;
				title.emviscount = 8;
				expiretimer(title.emtimer);
				expiretimer(title.texttimer);
			}
		}
	}
	
	if (title.state >= SHOWENEMIES)
	{
		// spawn new enemies
		if (enemies[title.emindex] != -1 && timeup(title.emtimer, EM_APPEAR_RATE))
		{
			spawn(enemies[title.emindex], EM_X<<CSF, em_y[title.emindex]<<CSF);
			title.emindex++;
			settimer(title.emtimer);
		}
		
		// update the text
		if (title.state==SHOWENEMIES)
		{
			if (timeup(title.texttimer, title.textupdrate))
			{
				settimer(title.texttimer);
				
				if (em_y[title.textindex] != -1)
				{
					++title.textindex;
				}
				else
				{
					title.copyright_visible = 1;
					title.state = SHOWTEXT;
					settimer(title.showtexttimer);
				}
			}
		}
			
		// animate the enemies
		doobjects();
	}
	
	if (title.state==SHOWTEXT)
	{
		if (timeup(title.showtexttimer, title.showtexttime))
		{
			title.showtexttime = 2100;
			title.textindex = 0;
			title.state = BLANKTEXT;
		}
	}
	else if (title.state==BLANKTEXT)
	{
		if (timeup(title.showtexttimer, 150))
		{
			// switch to showing points if showing names
			// and vice versa
			setemtext(title.emtextstate ^ 1);
			settimer(title.texttimer);
			title.state = SHOWENEMIES;
			title.textupdrate = TEXT_UPDATE_RATE;
		}
	}
	
	if (mousel && !lmousel)
	{
		initgame();
		gamemode = PLAY;
		freelogo();
	}
}

void drawtitle(void)
{
int i;

	fill(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, RGB(BGCOL));
	
	// logo
	drawlogo(LOGO_X, title.logoy);
	
	// blink the push fire to start
	if (timeup(title.pushfiretimer, 300))
	{
		settimer(title.pushfiretimer);
		title.pushfirevis ^= 1;
	}
	// draw "push fire to start"
	#define PF_X		((SCREEN_WIDTH/2)-((32*3)/2))
	if (title.pushfirevis) drawpushfire(PF_X-1, SCREEN_HEIGHT-80);
	
	// draw copyright
	if (title.copyright_visible)
		drawtext(txt_copyright, CPYRT_X, SCREEN_HEIGHT-40);
	
	// draw sample enemies
	drawobjs();
	
	// display text next to enemies
	for(i=0;i<title.textindex;i++)
	{
		drawtext(emtext[i], EM_TEXTX, em_y[i]);
	}
	
	drawhighscore();
	SDL_Flip(screen);
}

