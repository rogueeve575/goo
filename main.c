
#include "goo.h"
#include "synth.h"

#include "main.fdh"
object obj[MAX_OBJECTS];

char *highscorefile = "score.dat";
char fullscreen_active = 0;
SDL_Surface *screen = NULL;
player p;
uchar keys[SDLK_LAST], lastkeys[SDLK_LAST];
int mousex, mousey, mousel, mouser;
int lmousex, lmousey, lmousel, lmouser;
int curtick;
char quit = 0;
int blittimer, runtimer;
int po;
int gamemode;
stgame game;

#define MOUSE_SENS_ZOOM		1
#define MOUSE_SENS_NORM		2
int mouse_sens = MOUSE_SENS_NORM;

char txt_pause[5] = { 87,88,89,0 };
#define PAUSE_X		((SCREEN_WIDTH / 2) - (12 * 3))
#define PAUSE_Y		((SCREEN_HEIGHT / 2) - 12)

#define GMOV_OVER		1
#define GMOV_SCORE		2
#define GMOV_COUNT		3
#define GMOV_BLINK		4
#define GMOV_PUSHFIREWAIT	5
#define GMOV_PUSHFIRE		6

void SetMouseSens(int k)
{
	mouse_sens = k;
	if (!p.dead) setplayerx(obj[po].x);
}

void setgameover(void)
{
char tempbuf[800];
	if (!p.dead) kill(po);
	game.over = GMOV_OVER;
	settimer(game.gameovertimer);
	
	sprintf(tempbuf, "%04d", game.score);
	game.scorechars = strlen(tempbuf);
	sprintf(game.scorefmt, " %%0%dd", game.scorechars);
	game.score_x = (SCREEN_WIDTH/2)-((5*(TILE_SPACING/2))+(game.scorechars*(NUMBR_SPACING/2)));
	game.scoretally = 0;
	game.warpt_flashtimesleft = 0;
}

void doplayer(void)
{
	if (p.dead)
	{
		if (timeup(p.respawntimer, P_RESPAWN_TIME))
		{
			if (p.lives)
			{
				respawnplayer();
				setplayerx(p.diedatx);
			}
			else
			{
				setgameover();
			}
		}
		
		if (p.dead) return;
	}
	
	// player motion
	update_ppos_from_mouse();
	
	// player shooting
	if (mousel)
	{
		if (!lmousel) expiretimer(p.firetimer);
		if (timeup(p.firetimer, 100))
		{
			snd(FIRE);
			spawn(EM_PSHOT, obj[po].x, (game.dir==UP)?obj[po].y+P_H:obj[po].y-P_H);
			settimer(p.firetimer);
		}
	}
	
	// switch sides bomb
	if (mouser && !lmouser)
	{
		switchgamedir();
		SetMouseSens(MOUSE_SENS_ZOOM);
	}
	
	if (p.zooming)
	{
		#define ZOOMSPD		200
		if (p.zoomdir==DOWN)
		{
			obj[po].y += ZOOMSPD;
			if (obj[po].y >= P_BTMPOS)
			{
				obj[po].y = P_BTMPOS;
				setgamedir(DOWN);
				p.zooming = 0;
				SetMouseSens(MOUSE_SENS_NORM);
			}
		}
		else
		{
			obj[po].y -= ZOOMSPD;
			if (obj[po].y <= P_TOPPOS)
			{
				obj[po].y = P_TOPPOS;
				setgamedir(UP);
				p.zooming = 0;
				SetMouseSens(MOUSE_SENS_NORM);
			}
		}
	}
}

void update_ppos_from_mouse(void)
{
	obj[po].x = ((mousex - MOUSE_ADJ) * mouse_sens) << CSF;
	if (obj[po].x > MAXPX)
	{
		obj[po].x = MAXPX;
		SDL_WarpMouse(((MAXPX>>CSF)/mouse_sens)+MOUSE_ADJ, SCREEN_HEIGHT/2);
	}
	else if (obj[po].x < MINPX)
	{
		obj[po].x = MINPX;
		SDL_WarpMouse(((MINPX>>CSF)/mouse_sens)+MOUSE_ADJ, SCREEN_HEIGHT/2);
	}
	else
	{
		SDL_WarpMouse(mousex, SCREEN_HEIGHT/2);
	}
}

void addscore(int s)
{
char cantwarp = (game.score < PTS_TO_WARP);
	game.score += s;
	if (game.score < 0) game.score = 0;
	
	if (cantwarp && game.score >= PTS_TO_WARP)
	{	// just got warp bubble
		snd(CANWARPNOW);
		game.warpt_flashtimesleft = 24;
		settimer(game.warpt_timer);
		game.warpt_show = 1;
	}
}

void drawtext(char *buf, int x, int y)
{
	while(*buf)
	{
		draw(x, y, (*buf==' ')?1:*buf);
		if (*buf >= GetNumberTile(0) && *buf <= GetNumberTile(9))
			x += NUMBR_SPACING;
		else x += TILE_SPACING;
		buf++;
	}
}

void printscore(int score, int x, int y, char left)
{
char scorebuf[50];
int i;
	sprintf(scorebuf, "%04d", score);
	for(i=strlen(scorebuf)-1;i>=0;i--)
	{
		scorebuf[i] = GetNumberTile(scorebuf[i] - '0');
	}
	if (left)
	{
		x -= (strlen(scorebuf) * NUMBR_SPACING);
	}
	drawtext(scorebuf, x, y);
}

void newhighscore(int score)
{
	game.highscore = score;
	writehighscore(score);
	snd(LEVELUP);
}

void drawpushfire(int x, int y)
{
char arr[50];
int i;
	for(i=0;i<4;i++) arr[i] = 42+i;
	arr[i] = 0;
	drawtext(arr, x, y);
}

void drawhighscore(void)
{
	// show high-score
	printscore(game.highscore, SCREEN_WIDTH-7, SCREEN_HEIGHT-31, 1);
}

void drawscore(void)
{
char arr[50];
int i;
char scorebuf[50];
#define DX		8
#define DY		5

	// draw level
	arr[0] = 15; arr[1] = 16;
	if (game.level==9)		// level 10
	{
		arr[2] = GetNumberTile(1);
		arr[3] = GetNumberTile(0);
		arr[4] = 0;
	}
	else
	{
		arr[2] = GetNumberTile(game.level+1);
		arr[3] = 0;
	}
	drawtext(arr, DX, DY);
	
	// draw lives
	memset(arr, 14, p.lives);
	arr[p.lives] = 0;
	drawtext(arr, DX, DY+28);
	
	// draw score
	printscore(game.score, DX, SCREEN_HEIGHT-31, 0);
	drawhighscore();
	
	// "warp bubble" notice
	if (game.warpt_flashtimesleft)
	{
		if (timeup(game.warpt_timer, 50))
		{
			settimer(game.warpt_timer);
			if (!game.paused) game.warpt_flashtimesleft--;
			game.warpt_show ^= 1;
		}
		if (game.warpt_show)
		{
			#define WARPT_WD	123
			#define WARPT_X		((SCREEN_WIDTH / 2) - (WARPT_WD / 2))
			#define WARPT_Y		((SCREEN_HEIGHT / 2) - 12)
			extern char txt_warp[7];
			drawtext(txt_warp, WARPT_X, WARPT_Y);
		}
	}
	
	// game over
	if (game.over)
	{
		#define GMOV_Y	((SCREEN_HEIGHT/2)-(24/2)-26)
		#define GMOV_X	(SCREEN_WIDTH/2)-((24*6)/2)
		for(i=0;i<6;i++) arr[i] = 36+i;
		arr[i] = 0;
		drawtext(arr, GMOV_X, GMOV_Y);
		
		if (game.over >= GMOV_SCORE)
		{
			arr[0] = 18; arr[1] = 19; arr[2] = 20;
			if (game.over >= GMOV_COUNT && game.scoreblinkstate)
			{
				int len;
				sprintf(scorebuf, game.scorefmt, game.scoretally);
				len = strlen(scorebuf);
				arr[len+3] = 0;
				for(i=len-1;i>=0;i--)
				{
					arr[i+3] = GetNumberTile(scorebuf[i] - '0');
				}
			}
			else arr[3] = 0;
			drawtext(arr, game.score_x, GMOV_Y+38);
		}
		
		if (game.over >= GMOV_PUSHFIRE)
		{
			#define PF_X	(((SCREEN_WIDTH/2)-(5*(TILE_SPACING/2)))-4)
			drawpushfire(PF_X, GMOV_Y+80);
		}
	}
	
	if (game.paused)
	{
		if (timeup(game.pause_blinktimer, 400))
		{
			settimer(game.pause_blinktimer);
			game.pause_blinkstate ^= 1;
		}
		
		if (game.pause_blinkstate)
		{
			drawtext(txt_pause, PAUSE_X, PAUSE_Y);
		}
	}
}

void dogameover(void)
{
	switch(game.over)
	{
		case GMOV_OVER:
		case GMOV_SCORE:
			if (timeup(game.gameovertimer, 500))
			{
				settimer(game.gameovertimer);
				game.over++;
				game.scoreblinkstate = 1;
			}
		break;
		
		case GMOV_COUNT:
			if (game.scoretally < game.score)
			{
				int k;
				if (game.score >= 30000) k = 103;
				else if (game.score >= 10000) k = 34;
				else if (game.score >= 2500) k = 13;
				else if (game.score >= 1000) k = 7;
				else if (game.score >= 300) k = 3;
				else k = 1;
				
				game.scoretally += k;
				snd(FIRE);
			}
			else
			{
				game.scoretally = game.score;
				game.over = GMOV_BLINK;
				settimer(game.gameovertimer);
				game.scoreblinkcount = 5;
			}
		break;
		
		case GMOV_BLINK:
			if (timeup(game.gameovertimer, 300))
			{
				game.scoreblinkstate ^= 1;
				settimer(game.gameovertimer);
				if (!game.scoreblinkcount)
				{
					game.over++;
				}
				else
				{
					--game.scoreblinkcount;
				}
			}
		break;
		
		case GMOV_PUSHFIREWAIT:
			if (timeup(game.gameovertimer, 800))
			{
				game.over = GMOV_PUSHFIRE;
				if (game.score > game.highscore)
				{
					newhighscore(game.score);
				}
			}
		break;
		
		case GMOV_PUSHFIRE: break;
	}
	
	if (game.over >= GMOV_SCORE)
	{
		if (mousel && !lmousel)
		{
			if (game.over==GMOV_PUSHFIRE)
			{
				initgame();
			}
			else if (game.over != GMOV_PUSHFIREWAIT)
			{
				game.over = GMOV_PUSHFIREWAIT;
				game.scoretally = game.score;
				game.scoreblinkstate = 1;
				settimer(game.gameovertimer);
			}
		}
	}
}

void drawscene(void)
{
	fill(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, RGB(BGCOL));
	
	// blink player while he's invicible
	if (p.blinking)
	{
		if (++p.blinkanim>3)
		{
			p.blinkon ^= 1;
			p.blinkanim = 0;
			if (!game.paused && --p.blinktimesleft < 0) p.blinking = 0;
		}
	}
	else p.blinkon = 1;
	
	fld_draw_lower();
	drawobjs();
	fld_draw_upper();
	
	drawscore();
	
	SDL_Flip(screen);
}

void setplayerx(int x)
{
	if (p.dead) return;
	
	obj[po].x = x;
	mousex = ((x>>CSF) / mouse_sens) + MOUSE_ADJ;
	mousey = SCREEN_HEIGHT/2;
	SDL_WarpMouse(mousex, mousey);
	update_ppos_from_mouse();
}

void respawnplayer(void)
{
	p.dead = 0;
	expiretimer(p.firetimer);
	
	po = spawn(EM_PLAYER, 0, 0);
	setplayerx(((SCREEN_WIDTH << CSF) / 2) - (P_W / 2));
	p.zooming = 0;
	
	makepinvince(P_BLINK_TIMES);
	setgamedir(game.dir);
	SetMouseSens(MOUSE_SENS_NORM);
}

void makepinvince(int len)
{
	if (!p.blinking)
	{
		p.blinkon = 0;		// inverted first time to become 1
		p.blinking = 1;
		p.blinkanim = 0;
	}
	p.blinktimesleft = len;
}

void setgamedir(int newdir)
{
	game.dir = newdir;
	
	if (!p.dead)
	{
		if (game.dir==UP)
		{
			obj[po].y = P_TOPPOS;
			p.frame = P_DOWNFRAME;
		}
		else
		{
			obj[po].y = P_BTMPOS;
			p.frame = P_UPFRAME;
		}
	}
}

void switchgamedir(void)
{
	if (p.zooming) return;
	if (game.score < PTS_TO_WARP)
	{
		snd(CANTWARP);
		return;
	}
	addscore(-PTS_TO_WARP);
	
	snd(SWITCHSIDES);
	makepinvince(P_BLINK_TIMES);
	
	if (!p.zooming) p.zoomdir = game.dir;
	p.zoomdir ^= 1;
	if (p.zoomdir == DOWN)
	{
		p.frame = P_ZOOMDOWNFRAME;
		p.zooming = 1;
	}
	else
	{
		p.frame = P_ZOOMUPFRAME;
		p.zooming = 1;
	}
}

void initgame(void)
{
int savehighscore = game.highscore;
	
	memset(&game, 0, sizeof(game));
	game.highscore = savehighscore;
	game.dir = UP;
	
	obj_del_all();
	expiretimer(game.emtimer);
	
	fld_init();		// initilize "starfield"
	respawnplayer();
	p.lives = 3;
	settimer(blittimer);
	settimer(runtimer);
	SetMouseSens(MOUSE_SENS_NORM);
}

void gameloop(void)
{
	initgame();
	
	while(!quit)
	{
		synth_run();
		curtick = SDL_GetTicks();
		
		#define RUNRATE			10
		#define BLITRATE		(1000/60)
		if (timeup(runtimer, RUNRATE))
		{
			poll_events();
			
			while(timeup(runtimer, RUNRATE))
			{
				if (gamemode==TITLE)
				{
					runtitle();
				}
				else
				{
					if (!game.paused || game.over)
					{
						if (game.over)
							dogameover();
						else doplayer();
						
						if (p.dead && game.over < GMOV_PUSHFIREWAIT)
						{
							SDL_WarpMouse(SCREEN_WIDTH/2,SCREEN_HEIGHT/2);
						}
						
						spawnem();
						doobjects();
						fld_run();		// move "starfield"
					}
					
					if (!game.over)
					{
						if ((keys[SDLK_SPACE] && !lastkeys[SDLK_SPACE]) ||
							(keys[SDLK_p] && !lastkeys[SDLK_p]))
						{
							game.paused ^= 1;
							if (game.paused)
							{
								settimer(game.pause_blinktimer);
								game.pause_blinkstate = 1;
								snd(PAUSESND);
								
								if (!fullscreen_active)
								{
									SDL_ShowCursor(1);
									SDL_WarpMouse(SCREEN_WIDTH/2, SCREEN_HEIGHT-(SCREEN_HEIGHT/3));
								}
							}
							else
							{	// prevent cheating by pausing and moving the mouse
								if (!p.dead)
									setplayerx(obj[po].x);
								
								SDL_ShowCursor(0);
							}
							
							// prevent occasional double-pausing/un-pausing
							lastkeys[SDLK_p] = 1;
							lastkeys[SDLK_SPACE] = 1;
						}
					}
				}
				
				runtimer += RUNRATE;
			}
			
			memcpy(lastkeys, keys, sizeof(lastkeys));
			lmousex = mousex; lmousey = mousey;
			lmousel = mousel; lmouser = mouser;
		}
		
		if (timeup(blittimer, BLITRATE))
		{
			if (gamemode==TITLE) drawtitle();
			else drawscene();
			
			settimer(blittimer);
		}
	}
}


int readhighscore(void)
{
int a,b,c,d;
FILE *fp = fopen(highscorefile, "rb");
	if (!fp) { return 500; }
	
	a = fgetc(fp);
	b = fgetc(fp);
	c = fgetc(fp);
	d = fgetc(fp);
	fclose(fp);
	a <<= 24;
	b <<= 16;
	c <<= 8;
	a |= (b | c | d);
	return a;
}

void writehighscore(int h)
{
int a,b,c,d;
FILE *fp = fopen(highscorefile, "wb");
	if (!fp) { return; }
	
	a=b=c=d=h;
	a &= 0xff000000; a >>= 24;
	b &= 0x00ff0000; b >>= 16;
	c &= 0x0000ff00; c >>= 8;
	d &= 0x000000ff;
	fputc(a, fp);
	fputc(b, fp);
	fputc(c, fp);
	fputc(d, fp);
	fclose(fp);
}


void setup_screen(char fs)
{
Uint32 flags = SDL_SWSURFACE;
	
	if (fs) flags |= SDL_FULLSCREEN;
	fullscreen_active = fs;
	
	if (screen) SDL_FreeSurface(screen);
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, flags);
	
	if (fs)
	{
		SDL_ShowCursor(0);
	}
	else
	{
		if (game.paused) SDL_ShowCursor(1); else SDL_ShowCursor(0);
	}
}

void toggle_fullscreen(void)
{
	setup_screen(fullscreen_active ^ 1);
}

int main(int argc, char **argv)
{
	printf("initilizing goo invaders\n");
	#ifdef __HAIKU__
		MatchCurDir();
	#endif
	
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		printf("sdl_init failed"); return 1;
	}
	atexit( SDL_Quit );
	
	putenv("SDL_VIDEO_CENTERED=1");
	setup_screen(0);
	if (!screen) { printf("error setting video mode"); return 1; }
	
	SDL_WM_SetCaption("Aqua Invaders", NULL);
	
	if (loadart()) goto abort;
	if (synth_init()) goto abort;
	
	memset(keys, 0, sizeof(keys));
	memset(lastkeys, 0, sizeof(lastkeys));
	
	game.highscore = readhighscore();
	start_title();
	
	SDL_ShowCursor(0);
	fflush(stdout);
	gameloop();
	
	printf("normal exit\n");
	
abort: ;
//	SDL_WarpMouse(351, SCREEN_HEIGHT-20);
	synth_close();
	SDL_ShowCursor(1);
	freeart();
	freelogo();
	SDL_FreeSurface(screen);
	return 0;
}

static void poll_events()
{
SDL_Event evt;
int newState;

	while(SDL_PollEvent(&evt))
	{
		switch(evt.type)
		{
			case SDL_KEYDOWN:
				switch(evt.key.keysym.sym)
				{
					case SDLK_ESCAPE: quit = 1;
					break;
					
					/*case SDLK_f:
						toggle_fullscreen();
					break;
					
					case SDLK_RETURN:
						if (keys[SDLK_LALT] || keys[SDLK_RALT])
							toggle_fullscreen();
					break;*/
					
					default: break;
				}
				
				newState = 1;
				goto prockey;
			case SDL_KEYUP:
				newState = 0;
prockey: ;
				keys[evt.key.keysym.sym] = newState;
			break;
			
			case SDL_QUIT:
				quit = 1;
			break;
			
			case SDL_MOUSEMOTION:
				mousex = evt.motion.x;
				mousey = evt.motion.y;
				break;
			
			case SDL_MOUSEBUTTONDOWN:
				mousex = evt.button.x;
				mousey = evt.button.y;
				if (evt.button.button==SDL_BUTTON_LEFT)
					mousel = 1;
				else if (evt.button.button==SDL_BUTTON_RIGHT)
					mouser = 1;
				break;
				
			case SDL_MOUSEBUTTONUP:
				mousex = evt.button.x;
				mousey = evt.button.y;
				if (evt.button.button==SDL_BUTTON_LEFT)
					mousel = 0;
				else if (evt.button.button==SDL_BUTTON_RIGHT)
					mouser = 0;
				break;
		}
	}
}
