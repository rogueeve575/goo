
all:	goo

goo:  main.o art.o enemy.o snd.o \
	 title.o aquafld.o synth.o Blip_Buffer.o
	gcc -o goo \
	 main.o art.o enemy.o snd.o \
	 title.o aquafld.o synth.o Blip_Buffer.o \
	 -lstdc++ -lSDL -lSDLmain -lm

main.o:	main.c main.fdh goo.h snd.h \
		synth.h
	gcc -c main.c -o main.o

art.o:	art.c art.fdh goo.h snd.h \
		ART_RES.H LOGO_RES.H
	gcc -c art.c -o art.o

enemy.o:	enemy.c enemy.fdh goo.h snd.h
	gcc -c enemy.c -o enemy.o

snd.o:	snd.c snd.fdh goo.h snd.h \
		synth.h
	gcc -c snd.c -o snd.o

title.o:	title.c title.fdh goo.h snd.h
	gcc -c title.c -o title.o

aquafld.o:	aquafld.c aquafld.fdh goo.h snd.h
	gcc -c aquafld.c -o aquafld.o

synth.o:	synth.cpp
	gcc -c synth.cpp -o synth.o

Blip_Buffer.o:	Blip_Buffer.cpp
	gcc -c Blip_Buffer.cpp -o Blip_Buffer.o

.FORCE:

clean:
	rm -f main.o
	rm -f art.o
	rm -f enemy.o
	rm -f snd.o
	rm -f title.o
	rm -f aquafld.o
	rm -f synth.o
	rm -f Blip_Buffer.o
	rm -f goo

cleanfdh:
	rm -f main.fdh
	rm -f art.fdh
	rm -f enemy.fdh
	rm -f snd.fdh
	rm -f title.fdh
	rm -f aquafld.fdh
	rm -f synth.fdh
	rm -f Blip_Buffer.fdh

cleanall: clean cleanfdh

