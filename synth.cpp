
#include <stdio.h>
#include <SDL/SDL.h>
#include "Blip_Buffer.h"

#include <math.h>

//#define SCOPE
static Blip_Buffer     buf;
char quitme = 0;

// Amplitude Scale Factor. defines precision of computations.
// (it's used to do fake floating-point math using integers).
#define ASF			18
#define uchar		uint8_t
#define uint		uint16_t

extern "C"
{
	#include "synth.h"
}

// Sampling at higher than sample rate keeps aliasing high enough
// that Blip_Buffer filters most of it out.
const unsigned int sample_rate = 44100;
const unsigned int oversample = 4;
const unsigned int clocks_per_sec = sample_rate * oversample;

void mixaudio(void *unused, Uint8 *stream, int len);
static void sweepchannel(uchar c);
static void fadechannel(uchar c);

#define TONE_MINVOL		10


typedef struct			// square wave control
{
	double time;
	double period;
	uchar duty, duty_count;
} stSquare;

typedef struct			// triangle wave control
{
	unsigned int step;
	unsigned int phase;
} stTriangle;

typedef struct			// noise channel control
{
	double time, period;
} stNoise;


typedef struct
{
	char wavetype;
	
	unsigned int freq;
	int volume;
	
	int nextvol;
	int last_out;
	
	char sweep_enabled;
	int sweep_speed;
	
	char fade_enabled;
	int fade_speed;
	
	unsigned int sweeptimer, fadetimer;
	
	stSquare sq;
	stTriangle tri;
	stNoise noi;
	
	Blip_Synth<blip_good_quality,MAX_VOLUME> synth;
} stChannel;

stChannel chan[NUM_CHANNELS];


#ifdef SCOPE
#define SCOPE_LEN				30000
#define SCOPE_HEIGHT_REDUCE		4
typedef struct
{
	signed short sample;
	uchar mark_col;
} stScopeEntry;
stScopeEntry scope[SCOPE_LEN];
int scope_head;
int scope_trigtime;
char scope_triggered;
int scope_nextmarkcol;

void scope_init(void)
{
int i;
	printf("initilizing scope...\n");
	scope_head = 0;
	scope_trigtime = 0;
	scope_triggered = 0;
	scope_nextmarkcol = 0;
	for(i=0;i<SCOPE_LEN;i++)
	{
		scope[i].sample = 0;
		scope[i].mark_col = 200;
	}
}

void scope_mark(int col)
{
	scope_nextmarkcol = col;
}

void scope_trigger(int time)
{
	if (time)
	{
		scope_trigtime = time;
		printf("scope trigger armed\n");
		fflush(stdout);
	}
	else
	{
		printf("scope triggered (directly via scope_trigger)\n");
		scope_triggered = 1;
	}
}

void scope_write(void)
{
int x, y;
short val;
unsigned short zero;

	SDL_LockAudio();
	FILE *fp = fopen("c:\\temp\\wave.raw", "wb");
	
	printf("Writing scope...\n");
	fflush(stdout);
	
	zero = ((MAX_VOLUME / 2) / SCOPE_HEIGHT_REDUCE);
	for(y=0;y<=MAX_VOLUME/SCOPE_HEIGHT_REDUCE;y++)
	{
		x = scope_head;
		do
		{
			val = (scope[x].sample / SCOPE_HEIGHT_REDUCE);
			val += ((MAX_VOLUME / 2) / SCOPE_HEIGHT_REDUCE);
			if (val==y)
			{
				fputc(255, fp);
			}
			else if (y==zero)
			{
				fputc(100, fp);
			}
			else
			{
				fputc(scope[x].mark_col, fp);
			}
			
			if (++x >= SCOPE_LEN) x = 0;
		} while(x != scope_head);
	}
	
	printf("image x,y = %dx%d\n", SCOPE_LEN, MAX_VOLUME/SCOPE_HEIGHT_REDUCE);
	printf("done\n");
	fflush(stdout);
	quitme = 1;
	SDL_UnlockAudio();
}

void scope_add_sample(signed short smpl)
{
	if (scope_triggered) return;
	
	scope[scope_head].sample = smpl;
	scope[scope_head].mark_col = scope_nextmarkcol;
	scope_nextmarkcol = 0;
	if (++scope_head >= SCOPE_LEN) scope_head = 0;
	
	if (scope_trigtime)
	{
		if (!--scope_trigtime)
		{
			printf("scope triggered\n");
			scope_triggered = 1;
		}
	}
}

#endif


// changes current frequency of wave to "hz" hertz
void set_freq(uchar c, unsigned int hz)
{
	if (hz <= 0) hz = 1;
	chan[c].freq = hz;
	
	switch(chan[c].wavetype)
	{
		case WAVE_TRIANGLE:
		case WAVE_SAW:
			chan[c].tri.step = (hz << ASF) / clocks_per_sec;
		break;
			
		case WAVE_SQUARE:
			chan[c].sq.period = ((double)clocks_per_sec / (double)(hz)) / DUTY_STEPS;
		break;
		
		case WAVE_NOISE:
			// the 35 is arbitrary; it's just to get the noise channel
			// on the same frequency scale as the other waveforms
			chan[c].noi.period = ((double)clocks_per_sec / (double)(hz * 35));
		break;
	}
}


void run_triangle( uchar c, blip_time_t length )
{
int iphase;
int outvol;
	
	stTriangle *tri = &chan[c].tri;
	
	for(blip_time_t time = 0; time < length; time++)
	{
		tri->phase += tri->step;
		while ( tri->phase > (1<<ASF) )
			tri->phase -= (1<<ASF);
		
		iphase = (chan[c].volume * 4 * tri->phase) >> ASF;
		
		if ( iphase >= chan[c].volume * 2 )
			iphase = chan[c].volume * 4 - iphase;
		
		outvol = (iphase - chan[c].volume);
		
		// detect zero-crossings and apply any volume changes there
		// to eliminate popping
		if (chan[c].volume != chan[c].nextvol)
		{
			if ((outvol > 0 && chan[c].last_out < 0) || \
				(outvol < 0 && chan[c].last_out > 0) ||
				outvol==0)
			{
				#ifdef SCOPE
					scope_mark(128);
				#endif
				
				chan[c].volume = chan[c].nextvol;
				if (chan[c].volume < TONE_MINVOL) return;
			}
		}
		
		#ifdef SCOPE
			scope_add_sample(outvol);
		#endif
		chan[c].synth.update( time, outvol );
		chan[c].last_out = outvol;
	}
}


void run_saw( uchar c, blip_time_t length )
{
int iphase;
int outvol;
	
	stTriangle *tri = &chan[c].tri;
	
	for(blip_time_t time = 0; time < length; time++)
	{
		tri->phase += tri->step;
		while ( tri->phase > (1<<ASF) )
		{
			tri->phase -= (1<<ASF);
			chan[c].volume = chan[c].nextvol;
		}
		
		iphase = (chan[c].volume * 2 * tri->phase) >> ASF;
		iphase = (chan[c].volume * 2) - iphase;
		outvol = (iphase - chan[c].volume);
		
		#ifdef SCOPE
			scope_add_sample(outvol);
		#endif
		chan[c].synth.update( time, outvol );
		chan[c].last_out = outvol;
	}
}


void run_square( uchar c, blip_time_t length )
{
int outvol;
		
	while(chan[c].sq.time < length)
	{
		chan[c].sq.duty_count = (chan[c].sq.duty_count + 1) & DUTY_MASK;
sqrecalc: ;
		if (chan[c].sq.duty_count < chan[c].sq.duty)
			outvol = chan[c].volume;
		else
			outvol = -chan[c].volume;
		
		if (chan[c].nextvol != chan[c].volume && outvol != chan[c].last_out)
		{
			#ifdef SCOPE
				scope_mark(128);
			#endif
			chan[c].volume = chan[c].nextvol;
			if (chan[c].volume < TONE_MINVOL) return;
			goto sqrecalc;
		}
		
		#ifdef SCOPE
			scope_add_sample(outvol);
		#endif
		chan[c].synth.update((long)chan[c].sq.time, outvol);
		chan[c].sq.time += chan[c].sq.period;
		chan[c].last_out = outvol;
	}
	chan[c].sq.time -= length;
}


void run_noise(uchar c, int length)
{
int outvol;

	if (!chan[c].volume) return;
	stNoise *noi = &chan[c].noi;
	
	while(noi->time < length)
	{
		outvol = rand() % (chan[c].volume * 2) - chan[c].volume;
		
		chan[c].synth.update((long)noi->time, outvol);
		noi->time += noi->period;
	}
	noi->time -= length;
}


void make_sound(int length)
{
	for(int c=0;c<NUM_CHANNELS;c++)
	{
		if (chan[c].volume < TONE_MINVOL)
		{
			if (chan[c].wavetype != WAVE_NOISE) continue;
			if (chan[c].volume <= 0) continue;
		}
		if (chan[c].volume > MAX_VOLUME) continue;
		if (chan[c].freq < SYNTH_MIN_FREQ) continue;
		if (chan[c].freq > SYNTH_MAX_FREQ) continue;
		
		switch(chan[c].wavetype)
		{
			case WAVE_TRIANGLE: run_triangle(c, length); break;
			case WAVE_NOISE: run_noise(c, length); break;
			case WAVE_SQUARE: run_square(c, length); break;
			case WAVE_SAW: run_saw(c, length); break;
			default: break;
		}
	}
}

char init_sound(int samplerate)
{
SDL_AudioSpec fmt;

	fmt.freq = samplerate;
    fmt.format = AUDIO_S16;
    fmt.channels = 1;
    fmt.samples = 512;
    fmt.callback = mixaudio;
    fmt.userdata = NULL;

    // Open the audio device and start playing sound!
    if (SDL_OpenAudio(&fmt, NULL) < 0)
	{
        fprintf(stderr, "Unable to open audio: %s\n", SDL_GetError());
        return 1;
    }
	printf("init_sound: SDL audio opened at %d samples/sec\n", samplerate);
	return 0;
}

void mixaudio(void *unused, Uint8 *stream, int len)
{
short *streamptr = (short *)stream;
short buffer[50000];
int i;

	len /= 2;		// bytes -> words
	
	int num_samples = len * oversample;
	
	// synthesize output samples
	make_sound(num_samples);
	buf.end_frame(num_samples);
	
	// put samples into output buffer
	buf.read_samples(buffer, len);
	for(i=0;i<len;i++)
	{
		streamptr[i] = buffer[i];
	}
}


static void sweepchannel(uchar c)
{
int curtime = SDL_GetTicks();
	while(curtime - chan[c].sweeptimer >= SWEEP_RATE)
	{
		set_freq(c, chan[c].freq + chan[c].sweep_speed);
		chan[c].sweeptimer += SWEEP_RATE;
	}
}

static void fadechannel(uchar c)
{
int curtime = SDL_GetTicks();
	while(curtime - chan[c].fadetimer >= FADE_RATE)
	{
		synth_set_vol(c, chan[c].nextvol + chan[c].fade_speed);
		chan[c].fadetimer += FADE_RATE;
	}
}


// C interface ------------------------------------------------------------
extern "C"
{

char synth_init(void)
{
int i;

	if (init_sound(sample_rate))
	{
		printf("synth_init: sound initilization failed\n");
		return 1;
	}
	
	printf("synthesizer: initilization in progress...\n");
	
	buf.clock_rate( clocks_per_sec );
	if ( buf.set_sample_rate( sample_rate ) )
	{
		printf("synth_init: Out of Memory!!\n");
		return 1;
	}
	
	for(i=0;i<NUM_CHANNELS;i++)
	{
		chan[i].wavetype = WAVE_SQUARE;
		chan[i].volume = chan[i].nextvol = 0;
		chan[i].tri.phase = 0;
		chan[i].last_out = 0;
		chan[i].sq.time = 0;
		chan[i].sq.duty = DUTY_50;
		chan[i].sq.duty_count = 0;
		chan[i].sq.period = 44.10f;		//500hz
		chan[i].noi.time = 0;
		chan[i].freq = 500;
		chan[i].fade_enabled = chan[i].sweep_enabled = 0;
		
		chan[i].synth.volume( 0.5 );
		chan[i].synth.treble_eq ( -100 );
		chan[i].synth.output( &buf );
	}
	
	#ifdef SCOPE
		scope_init();
	#endif

    SDL_PauseAudio(0);
	return 0;
}

void synth_close(void)
{
	#ifdef SCOPE
		scope_write();
	#endif
	
	SDL_CloseAudio();
}

void synth_run(void)
{
int i;

	for(i=0;i<NUM_CHANNELS;i++)
	{
		if (channel_in_use(i))
		{
			if (chan[i].sweep_enabled) sweepchannel(i);
			if (chan[i].fade_enabled) fadechannel(i);
		}
	}
}

void tone_on(uchar c, unsigned int freq, int vol)
{
	set_freq(c, freq);
	if (chan[c].volume != vol) synth_set_vol(c, vol);
	//synth_set_duty(c, duty_cycle);
	chan[c].fade_enabled = chan[c].sweep_enabled = 0;
	chan[c].sweeptimer = chan[c].fadetimer = SDL_GetTicks();
}

void tone_off(uchar c)
{
	chan[c].volume = 0;
}

void tone_sweep(uchar c, int sweep_speed)
{
	if (!sweep_speed)
	{
		chan[c].sweep_enabled = 0;
	}
	else
	{
		chan[c].sweep_speed = sweep_speed;
		if (!chan[c].sweep_enabled)
		{
			chan[c].sweep_enabled = 1;
			chan[c].sweeptimer = SDL_GetTicks();
		}
	}
}

void tone_fade(uchar c, int fade_speed)
{
	if (!fade_speed)
	{
		chan[c].fade_enabled = 0;
	}
	else
	{
		chan[c].fade_speed = fade_speed;
		if (!chan[c].fade_enabled)
		{
			chan[c].fade_enabled = 1;
			chan[c].fadetimer = SDL_GetTicks();
		}
	}
}

void synth_set_vol(uchar c, int vol)
{
	if (chan[c].volume < TONE_MINVOL || chan[c].wavetype==WAVE_NOISE)
	{
		chan[c].volume = vol;
		chan[c].nextvol = vol;
	}
	else
	{
		chan[c].nextvol = vol;
		
		#ifdef SCOPE
			scope_mark(50);
		#endif
		
//		while(chan[c].volume != chan[c].nextvol);
	}
}

void synth_set_wavetype(uchar c, char w)
{
	SDL_LockAudio();
		chan[c].wavetype = w;
		set_freq(c, chan[c].freq);
	SDL_UnlockAudio();
}

void synth_set_duty(uchar c, uchar duty)
{
	chan[c].sq.duty = duty;
}

// returns whether the channel's last requested volume has been applied yet
char synth_vol_applied(uchar c)
{
	return (chan[c].volume==chan[c].nextvol);
}

// returns true if given channel no is currently playing sound
char channel_in_use(uchar c)
{
	if (chan[c].volume > 0)
		return 1;
	else
		return 0;
}

// returns an available channel no, or -1 if all are filled.
char synth_get_free_channel(void)
{
	for(int i=0;i<NUM_CHANNELS;i++)
	{
		if (!channel_in_use(i)) return i;
	}
	return -1;
}

}
