
## WELCOME TO AQUA INVADERS FROM THE PLANET GOOBAR!

### YOUR BASE IS UNDER ATTACK. THE AQUA INVADERS FROM THE PLANET GOOBAR ARE SWARMING ON YOU AND EVERYBODY ELSE IS BUSY DRINKING MILKSHAKES! BESIDES, YOU'RE THE ONLY ONE WITH A LASER CANNON AND YOUR OWN SUBMARINE. LOOKS LIKE ITS UP TO YOU AGAIN. IT'S PROBABLY HOPELESS, BUT HOLD THEM OFF LONG ENOUGH AND YOUR NAME COULD LIVE ON FOREVER.


Use the mouse to control your ship.

The left mouse button fires your weapon.

The right mouse button activates your ship's Warp Bubble. Unfortunately, it costs 400 points to use it.

Pressing SPACEBAR or P will pause the game.

Press ESC to quit.


Getting it running:

The game should build on Linux with just a "make".

This game was also built under Haiku pre-alpha 32645, gcc2 build. It should theoretically run under BeOS as well but this was not tested.

This game requires the SDL 1.2 libraries. It was tested with SDL 1.2.13.

If you do not have SDL yet you will need to download "libpak" or some other package which provides the SDL libraries, and copy at least "libSDL.so" into /boot/system/lib or some other place where they can be found by the system (Haiku-specific; on Linux just install SDL normally from your package manager e.g. sudo pacman -S sdl12-compat).

http://www.fileden.com/files/2008/8/23/2062382/packages/SDL-1.2.13-haiku-2.zip

There are no other dependencies.



Program
(c)Caitlin Shaw 2008-2009

